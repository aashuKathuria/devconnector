const express = require('express');
const mongo = require('./config/db');

const server = express();
mongo.dbConnect();

const port = process.env.PORT || 5000;
server.listen(port, () => console.log(`Server running at port 5000`));
server.use(express.json({ extended: false }));
server.get('/', (req, res) => res.send(`Hello`));
// server.use((req, res, err) => console.log(`error`, err));
server.use('/api/user', require('./routes/api/users'));
server.use('/api/profile', require('./routes/api/profile'));
server.use('/api/posts', require('./routes/api/posts'));
server.use('/api/auth', require('./routes/api/auth'));

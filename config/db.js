const mongoose = require('mongoose');
const config = require('config');

const dbConnect = async () => {
  try {
    await mongoose.connect(config.get('db'), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndexes: true,
      useFindAndModify: false,
    });
    console.log(`db Connected`);
  } catch (e) {
    console.log(`Db Connection Error`, e);
    process.exit();
  }
};

module.exports = { dbConnect };

const express = require('express');
const { check, validationResult } = require('express-validator');
const config = require('config');
const request = require('request');
const auth = require('./../../middleware/auth');
const Profile = require('./../../models/Profile');
const Post = require('./../../models/Post');
const router = express.Router();

//@route  GET api/profile/me
//@desc   Current User Profile
//@access Private
router.get('/me', auth, async (req, res) => {
  try {
    let profile = await Profile.findOne({ user: req.user.id }).populate(
      'user',
      ['avatar', 'name']
    );

    if (!profile) {
      return res.status(400).json({ msg: 'Profile not found' });
    }
    return res.json(profile);
  } catch (err) {
    console.error(err.message);
    return res.status(500).json({ msg: 'Profile not found' });
  }
});

//@route  POST api/profile/
//@desc   Create & Update User Profile
//@access Private
router.post(
  '/',
  [
    auth,
    [
      check('status', 'Status is Required').not().isEmpty(),
      check('skills', 'Skills is required').not().isEmpty(),
    ],
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      let profile = await Profile.findOne({
        user: req.user.id,
      });

      const {
        company,
        website,
        location,
        status,
        skills,
        bio,
        githubusername,
        facebook,
        youtube,
        linkedin,
        twitter,
        instagram,
      } = req.body;

      const profileObject = {};
      profileObject.user = req.user.id;
      if (company) profileObject.company = company;
      if (website) profileObject.website = website;
      if (location) profileObject.location = location;
      if (status) profileObject.status = status;
      if (skills) profileObject.skills = skills.split(',').map((v) => v.trim());
      if (bio) profileObject.bio = bio;
      if (githubusername) profileObject.githubusername = githubusername;

      profileObject.social = {};
      if (youtube) profileObject.social.youtube = youtube;
      if (facebook) profileObject.social.facebook = facebook;
      if (twitter) profileObject.social.twitter = twitter;
      if (linkedin) profileObject.social.linkedin = linkedin;
      if (instagram) profileObject.social.instagram = instagram;

      if (profile) {
        const profile = await Profile.findOneAndUpdate(
          { user: req.user.id },
          profileObject,
          {
            new: true,
          }
        );
        return res.json(profile);
      }
      profile = new Profile(profileObject);
      await profile.save();
      return res.json(profile);
    } catch (err) {
      console.error(err.message);
      return res.status(500).send('Server Error');
    }
  }
);

//@route  GET api/profile
//@desc   Get All User Profile
//@access Private
router.get('/', async (req, res) => {
  try {
    const profile = await Profile.find().populate('user', ['avatar', 'name']);

    return res.json(profile);
  } catch (err) {
    console.error(err.message);
    return res.status(500).json({ msg: 'Server Error' });
  }
});

//@route  GET api/profile/user/:userId
//@desc   Get User Profile by UserId
//@access Private
router.get('/user/:userId', auth, async (req, res) => {
  try {
    const profile = await Profile.findOne({
      user: req.params.userId,
    }).populate('user', ['avatar', 'name']);

    if (!profile) {
      res.status(400).json({ msg: 'There is no profile with this userId' });
    }
    return res.json(profile);
  } catch (err) {
    console.error(err.message);
    return res.status(500).json({ msg: 'Server Error' });
  }
});

//@route  Delete api/profile/
//@desc   Delete Profile,User
//@access Private
router.delete('/', auth, async (req, res) => {
  try {
    await Post.deleteMany({ user: req.user.id });
    await User.findOneAndRemove({
      _id: req.user.id,
    });
    await Profile.findOneAndRemove({
      user: req.user.id,
    });

    return res.json({ msg: 'Successfully deleted' });
  } catch (err) {
    console.error(err.message);
    return res.status(500).json({ msg: 'Server Error' });
  }
});

//@route Add /experience
//@desc Add experience
//@access Private
router.put(
  '/experience',
  [
    auth,
    [
      check('title', 'Title is required').not().isEmpty(),
      check('company', 'Company is Required').not().isEmpty(),
      check('from', 'From date is required').not().isEmpty(),
    ],
  ],
  async (req, res) => {
    const error = validationResult(req);

    if (!error.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const {
      title,
      company,
      location,
      from,
      to,
      current,
      description,
    } = req.body;

    const newExp = { title, company, from, location, to, current, description };

    try {
      const profile = await Profile.findOne({ user: req.user.id });

      if (!profile) {
        return res.status(400).json({ msg: 'No Profile found' });
      }

      profile.experience.unshift(newExp);
      await profile.save();
      return res.json(profile);
    } catch (err) {
      console.error(err);
      res.status(500).json({ msg: 'Server Error' });
    }
  }
);

//@route /experience/:exp_id
//@desc delete experience
//@access private
router.delete('/experience/:exp_id', [auth], async (req, res) => {
  try {
    const profile = await Profile.findOne({ user: req.user.id });

    if (!profile) {
      return res.status(400).json({ msg: 'Profile not found' });
    }
    const removeId = profile.experience
      .map((v, i) => {
        return v._id;
      })
      .indexOf(req.params.exp_id);

    if (removeId != -1) {
      profile.experience.splice(removeId, 1);
      await profile.save();
    }
    return res.json(profile);
  } catch (err) {
    console.error(err);
    return res.status(500).json({ msg: 'Server Error' });
  }
});

//@route Add /education
//@desc Add education
//@access Private
router.put(
  '/education',
  [
    auth,
    [
      check('school', 'Title is required').not().isEmpty(),
      check('degree', 'Company is Required').not().isEmpty(),
      check('fieldofstudy', 'field of study is Required').not().isEmpty(),
      check('from', 'From date is required').not().isEmpty(),
    ],
  ],
  async (req, res) => {
    const error = validationResult(req);

    if (!error.isEmpty()) {
      return res.status(400).json({ errors: error.array() });
    }
    const {
      school,
      degree,
      fieldofstudy,
      from,
      to,
      current,
      description,
    } = req.body;

    const newEdu = {
      school,
      degree,
      fieldofstudy,
      from,
      to,
      current,
      description,
    };

    try {
      const profile = await Profile.findOne({ user: req.user.id });

      if (!profile) {
        return res.status(400).json({ msg: 'No Profile found' });
      }

      profile.education.unshift(newEdu);
      await profile.save();
      return res.json(profile);
    } catch (err) {
      console.error(err);
      res.status(500).json({ msg: 'Server Error' });
    }
  }
);

//@route /education/:edu_id
//@desc delete education
//@access private
router.delete('/education/:edu_id', [auth], async (req, res) => {
  try {
    const profile = await Profile.findOne({ user: req.user.id });

    if (!profile) {
      return res.status(400).json({ msg: 'Profile not found' });
    }
    const removeId = profile.education
      .map((v, i) => {
        return v._id;
      })
      .indexOf(req.params.edu_id);

    if (removeId != -1) {
      profile.education.splice(removeId, 1);
      await profile.save();
    }
    return res.json(profile);
  } catch (err) {
    console.error(err);
    return res.status(500).json({ msg: 'Server Error' });
  }
});

//@route /github/:user_name
//@desc get User Github Profile
//@access Public
router.get('/github/:user_name', (req, res) => {
  try {
    const options = {
      uri: `https://api.github.com/users/${
        req.params.user_name
      }/repos?per_page=1&sort=created:asc&client_id=${config.get(
        'githubId'
      )}&client_secret=${config.get('githubSecret')}`,
      method: 'get',
      headers: { 'user-agent': 'node.js' },
    };
    request(options, (error, response, body) => {
      if (error) console.error(error);

      if (response.statusCode != 200)
        return res.status(404).json({ msg: 'No Github Profile Found' });

      return res.json(JSON.parse(body));
    });
  } catch (err) {}
});
module.exports = router;

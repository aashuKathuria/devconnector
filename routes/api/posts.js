const express = require('express');
const { check, validationResult } = require('express-validator');
const auth = require('./../../middleware/auth');
const checkObjectId = require('./../../middleware/checkObjectId');
const Post = require('./../../models/Post');
const User = require('./../../models/User');
const Profile = require('./../../models/Profile');
const router = express.Router();

//@route  POST api/post
//@desc   Add Post Route
//@access Private
router.post(
  '/',
  [auth, [check('text', 'Text is required').not().isEmpty()]],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).send({ errors: errors.array() });
    }
    try {
      const user = await User.findById(req.user.id).select('-password');

      let post = new Post({
        text: req.body.text,
        avatar: user.avatar,
        name: user.name,
        user: req.user.id,
      });

      post = await post.save();
      return res.send(post);
    } catch (error) {
      console.error(error.message);
      return res.status(500).send('Server Error');
    }
  }
);

//@route GET api/posts
//@desc Get POsts
//@access Private
router.get('/', [auth], async (req, res) => {
  try {
    const post = await Post.find().sort({ date: -1 });
    if (!post) {
      return res.status(404).json({ msg: 'No Post found' });
    }
    return res.json(post);
  } catch (error) {
    console.error(error.message);
    return res.status(500).send('Server Error');
  }
});

//@route GET api/posts/:post_id
//@desc Get POst
//@access Private
router.get('/:post_id', [auth], async (req, res) => {
  try {
    const post = await Post.findById(req.params.post_id);
    if (!post) {
      return res.status(404).json({ errors: [{ msg: 'No Post found' }] });
    }
    return res.json(post);
  } catch (error) {
    console.error(error.message, '$$$$$$$$$$$$$$$$$$$$$$');
    return res.status(500).send({ errors: [{ msg: 'Server Error' }] });
  }
});

//@route DELETE api/posts/:post_id
//@desc Delete Posts
//@access Private
router.delete('/:post_id', [auth, checkObjectId('id')], async (req, res) => {
  try {
    const post = await Post.findById(req.params.post_id);
    if (!post) {
      return res.status(404).json({ msg: 'No Post found' });
    }

    if (post.user.toString() != req.user.id) {
      return res
        .status(401)
        .json({ errors: [{ msg: 'User not authourized' }] });
    }
    await post.delete();
    return res.json({ errors: [{ msg: 'Post deleted successfully' }] });
  } catch (error) {
    console.error(error.message);
    return res.status(500).send('Server Error');
  }
});

//@route Post api/posts/like/:post_id
//@desc Add Likes
//@access Private
router.post('/like/:post_id', [auth, checkObjectId('id')], async (req, res) => {
  try {
    const post = await Post.findById(req.params.post_id);
    if (post.likes.some((v) => v.user.toString() === req.user.id)) {
      return res.status(400).json({ errors: [{ msg: 'Already Liked' }] });
    }

    post.likes.unshift({ user: req.user.id });

    await post.save();
    return res.json(post.likes);
  } catch (error) {
    console.error(error.message);
    return res.status(500).send('Server Error');
  }
});

//@route Delete api/posts/like/:post_id
//@desc Remove Likes
//@access Private
router.delete(
  '/like/:post_id',
  [auth, checkObjectId('id')],
  async (req, res) => {
    try {
      const post = await Post.findById(req.params.post_id);
      if (!post.likes.some((v) => v.user.toString() === req.user.id)) {
        return res.status(400).json({ errors: [{ msg: 'Not Liked' }] });
      }

      const removeIndex = post.likes.filter((like, i) => {
        if (like.user === req.user.id) return i;
      });
      post.likes.splice(removeIndex, 1);
      await post.save();
      return res.json(post.likes);
    } catch (error) {
      console.error(error.message);
      return res.status(500).send('Server Error');
    }
  }
);

//@route Post api/posts/comment/:post_id
//@desc Add comments
//@access Private
router.post(
  '/comment/:post_id',
  [
    auth,
    checkObjectId('post_id'),
    [check('text', 'Text is required').not().isEmpty()],
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    try {
      const post = await Post.findById(req.params.post_id);
      const user = await User.findById(req.user.id).select('-password');

      post.comments.unshift({
        user: req.user.id,
        avatar: user.avatar,
        name: user.name,
        text: req.body.text,
      });

      await post.save();
      return res.json(post.comments);
    } catch (error) {
      console.error(error.message);
      return res.status(500).send('Server Error');
    }
  }
);

//@route Delete api/posts/comment/:post_id/:comment_id
//@desc Remove comments
//@access Private
router.delete('/comment/:post_id/:comment_id', auth, async (req, res) => {
  try {
    const post = await Post.findById(req.params.post_id);

    const comment = post.comments.find(
      (comment) => comment.id === req.params.comment_id
    );
    if (!comment) {
      return res.status(404).json({ msg: 'No Comment found' });
    }
    if (comment.user.toString() !== req.user.id) {
      return res.status(401).json({ msg: 'User not authorized' });
    }
    post.comments = post.comments.filter(
      ({ id }) => id !== req.params.comment_id
    );
    await post.save();
    return res.json(post.comments);
  } catch (error) {
    console.error(error.message);
    return res.status(500).send('Server Error');
  }
});

module.exports = router;

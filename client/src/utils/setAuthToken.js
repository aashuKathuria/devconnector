import axios from 'axios';

export const setAuthToken = (token) => {
  // console.log(token, 'setAuthToken');
  if (token) {
    axios.defaults.headers.common = {
      'x-auth-token': token,
    };
  } else {
    // console.log('Delete Called');
    delete axios.defaults.headers.common;
  }
};

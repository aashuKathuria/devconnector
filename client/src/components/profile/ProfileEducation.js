import React from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';

const ProfileEducation = ({
  edu: { school, degree, from, to, description, current, fieldofstudy },
}) => {
  return (
    <div>
      <h3>{school}</h3>
      <Moment format='MM YYYY'>{from}</Moment> -{' '}
      {current ? 'Current' : <Moment format='MM YYYY'>{to}</Moment>}
      <p>
        <strong>Degree: </strong>
        {degree}
      </p>
      <p>
        <strong>Field Of Study: </strong>
        {fieldofstudy}
      </p>
      <p>
        <strong>Description: </strong>
        {description}
      </p>
    </div>
  );
};

ProfileEducation.propTypes = {
  edu: PropTypes.object.isRequired,
};

export default ProfileEducation;

import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import { addPost } from './../../actions/post';
import { connect } from 'react-redux';

const PostForm = ({ addPost }) => {
  const [formData, setFormData] = useState({
    text: '',
  });
  const { text } = formData;

  const onChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    setFormData({ text: '' });
    addPost({ formData });
  };
  return (
    <Fragment>
      <div className='post-form'>
        <div className='bg-primary p'>
          <h3>Say Something...</h3>
        </div>
        <form className='form my-1' onSubmit={(e) => onSubmit(e)}>
          <textarea
            value={text}
            onChange={(e) => onChange(e)}
            name='text'
            cols='30'
            rows='5'
            placeholder='Create a post'
            required
          ></textarea>
          <input type='submit' className='btn btn-dark my-1' value='Submit' />
        </form>
      </div>
    </Fragment>
  );
};

PostForm.propTypes = {
  addPost: PropTypes.func.isRequired,
};

export default connect(null, { addPost })(PostForm);

import React, { Fragment, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { getPost } from './../../actions/post';
import Spinner from './../../components/layout/Spinner';
import CommentForm from './CommentForm';
import Comments from './Comments';

const Post = ({
  getPost,
  post: {
    loading,
    post,
    // post: { avatar, text, name },
  },
  match,
}) => {
  useEffect(() => {
    getPost(match.params.id);
  }, [getPost, match.params.id]);

  return (
    <Fragment>
      {loading || !post ? (
        <Spinner />
      ) : (
        <Fragment>
          <Link to='/posts' className='btn'>
            Back To Posts
          </Link>
          <div className='post bg-white p-1 my-1'>
            <div>
              <Link to='/profile'>
                <img className='round-img' src={post.avatar} alt='' />
                <h4>{post.name}</h4>
              </Link>
            </div>
            <div>
              <p className='my-1'>{post.text}</p>
            </div>
          </div>

          <CommentForm post={post._id} />
          <Comments comments={post.comments} postId={post._id} />
        </Fragment>
      )}
    </Fragment>
  );
};

const mapStateToProps = (state) => ({
  post: state.post,
});
export default connect(mapStateToProps, { getPost })(Post);

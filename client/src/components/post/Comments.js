import React, { Fragment, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { deleteComment } from './../../actions/post';
import Spinner from './../../components/layout/Spinner';
import Moment from 'react-moment';
import PropTypes from 'prop-types';

const Comments = ({ comments, auth, deleteComment, postId }) => {
  return (
    <Fragment>
      {false ? (
        <Spinner />
      ) : (
        <Fragment>
          <div className='comments'>
            {comments.map((comment) => (
              <Fragment key={comment._id}>
                <div className='post bg-white p-1 my-1'>
                  <div>
                    <Link to='/profile'>
                      <img className='round-img' src={comment.avatar} alt='' />
                      <h4>{comment.name}</h4>
                    </Link>
                  </div>
                  <div>
                    <p className='my-1'>{comment.text}</p>
                    <p className='post-date'>
                      Posted on{' '}
                      <Moment format='DD/MM/YYYY'>{comment.date}</Moment>
                    </p>
                    {!auth.loading &&
                      auth.user._id === comment.user.toString() && (
                        <button
                          type='button'
                          className='btn btn-danger'
                          onClick={(e) => deleteComment(postId, comment._id)}
                        >
                          <i className='fas fa-times'></i>
                        </button>
                      )}
                  </div>
                </div>
              </Fragment>
            ))}
          </div>
        </Fragment>
      )}
    </Fragment>
  );
};
Comments.propTypes = {
  comments: PropTypes.array.isRequired,
  postId: PropTypes.string.isRequired,
};
const mapStateToProps = (state) => ({
  auth: state.auth,
});
export default connect(mapStateToProps, { deleteComment })(Comments);

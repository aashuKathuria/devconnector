import {
  REGISTER_FAIL,
  REGISTER_SUCCESS,
  USER_LOADED,
  AUTH_ERROR,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
  CLEAR_PROFILE,
  ACCOUNT_DELETED,
} from './types';
import { setAuthToken } from './../utils/setAuthToken';
import { setAlert } from './../actions/alert';
import axios from 'axios';

export const register = ({ name, email, password }) => async (dispatch) => {
  const config = {
    headers: {
      'content-type': 'application/json',
    },
  };

  const body = {
    name,
    email,
    password,
  };

  try {
    const res = await axios.post(
      '/api/user/register',
      JSON.stringify(body),
      config
    );
    dispatch({ type: REGISTER_SUCCESS, payload: { token: res.data.token } });
  } catch (error) {
    const errors = error.response.data.errors;

    if (errors) {
      errors.forEach((error) => dispatch(setAlert(error.msg, 'danger')));
    }

    console.error(error);
    dispatch({ type: REGISTER_FAIL });
  }
};

export const loadUser = () => async (dispatch) => {
  try {
    const res = await axios.get('api/auth');
    dispatch({ type: USER_LOADED, payload: res.data });
  } catch (error) {
    console.error(error);
    dispatch({ type: AUTH_ERROR });
  }
};

export const login = (email, password) => async (dispatch) => {
  try {
    const config = {
      headers: {
        'content-type': 'application/json',
      },
    };
    const body = { email, password };
    const res = await axios.post('api/auth', JSON.stringify(body), config);

    dispatch({ type: LOGIN_SUCCESS, payload: res.data });
    setAuthToken(res.data.token);
    dispatch(loadUser());
  } catch (error) {
    dispatch({ type: LOGIN_FAIL });
  }
};

export const logout = () => async (dispatch) => {
  dispatch({ type: CLEAR_PROFILE });
  dispatch({ type: LOGOUT });
};

export const deleteAccount = (id) => async (dispatch) => {
  if (window.confirm('Are you sure?')) {
    try {
      await axios.delete(`/api/profile/`);
      dispatch({ type: ACCOUNT_DELETED });
      dispatch(setAlert('Account deleted successfully', 'success'));
    } catch (error) {
      dispatch({
        type: AUTH_ERROR,
        payload: {
          msg: error.response.statusText,
          status: error.response.status,
        },
      });
    }
  }
};
